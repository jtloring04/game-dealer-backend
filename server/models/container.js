module.exports = (Container) => {
  Container.beforeRemote("download", async (ctx) => {
    const { req, res } = ctx;
    if (req.query.download) {
      res.setHeader("Content-Description", "File Transfer");
      res.setHeader("Content-Type", "application/octet-stream");
      res.setHeader("Content-Disposition", "attachment");
    }
  });
};
