const bodyParser = require("body-parser");
const axios = require("axios");

const {
  baseUrl,
  api_key,
  integration_id,
  expiration,
  currency,
} = require("../config.payment");

const dataExample = require("../helpers/payment.request.example");

module.exports = async (app) => {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.post("/api/checkout", async (req, res) => {
    const { order_id, amount_cents, items, billing_data } = req.body;

    try {
      const { data: merchantData } = await axios.post(
        `${baseUrl}/auth/tokens`,
        {
          api_key,
        }
      );

      const {
        token: auth_token,
        profile: { id: merch_id },
      } = merchantData;

      const { data: orderData } = await axios.post(
        `${baseUrl}/ecommerce/orders`,
        {
          auth_token,
          delivery_needed: "false",
          merch_id,
          amount_cents,
          currency,
          merchant_order_id: order_id,
          items,
        }
      );

      const { data: paymentData } = await axios.post(
        `${baseUrl}/acceptance/payment_keys`,
        {
          auth_token,
          amount_cents,
          expiration,
          currency,
          integration_id,
          billing_data,
        }
      );

      return { tokne: paymentData.token };
    } catch (error) {
      console.log(error.reponse.data.message);
    }
  });
};
